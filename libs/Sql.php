<?php

Class Sql
{
    protected $select;
    protected $update;
    protected $delete;
    protected $insert;
    protected $where = [];
    protected $values;
    protected $from;
    protected $set;
    protected $message;

    protected function __construct() {

        $this->message = new Message();

    }
    public function select(...$select)
    {
        $this->select = $this->prepareData($select, 'a');
        return $this;
    }

    public function update($update)
    {    
        $this->update = $this->prepareData($update);
        return $this;
    }

    public function delete($delete)
    {
        $this->delete = $this->prepareData($delete);
        return $this;
    }

    public function insert($tableName, $insert = null)
    {
        $tableName = (string) $this->prepareData($tableName);
        if (!$tableName)
        {
            return false;
        }

        $this->insert = $tableName . ' ' . $this->prepareData($insert, 'a');
        return $this;
    }

    public function from($from)
    {
        $this->from = $this->prepareData($from);
        return $this;
    }

    public function values(array $values)
    {
        $this->values = $this->prepareData($values, 'a');
        return $this;
    }

    public function set(array $set)
    {
        $this->set = $this->prepareData($set, 'as');
        return $this;
    }
    
    public function where($field, $operator, $value)
    {
        if (empty($field) or empty($operator))
        {
            return false;
        }
        if ($operator == 'IN')
        {
            if (!is_array($value))
            {
                return false;
            }
            $value = '(' . $this->prepareData($value, 'a') . ') ';
        }
        elseif (is_string($value))
        {
            $value = '"' . (string) $value . '"';
        }
        elseif (!is_bool($value) or $value !== NULL or !is_numeric($value))
        {
            return false;
        }
        $this->where[] = $this->prepareData($field) . ' ' . $this->prepareData($operator) . ' ' . $value;
        return $this;
    }

    protected function prepareData($data, $type = 's', $sep = ', ')
    {
        switch ($type)
        {
            case 's':
                $data = (string) $data;
                    return $data;

            case 'a':
                if (is_array($data) and count($data))
                {
                    $data = implode($sep, $data);
                }
                else
                {
                    return false;
                }
                return $data;
            case 'as':

                if (is_array($data) and count($data))
                {
                    $res = '';
                    array_walk($data, function($val, $key) use (&$res)
                    {
                        $res .= $key . '=' . $val . ', ';
                    });
                }
                else
                {
                    return false;
                }
                return substr($res, 0, -2) . ' ';
            default:
                return $data;
        }
    }

   
    public function exec()
    {
        switch (true)
        {
            case (isset($this->select)):
                if (empty($this->select) or $this->select == '*')
                {
                    return $this->message->setMessage('Empty select', 2);
                }
                $query = 'SELECT ' . $this->select  . ' ';
                    break;
            case (isset($this->insert)):
                if (empty($this->insert))
                {
                    return $this->message->setMessage('You missed argument for insert', 2);
                }
                $query = 'INSERT INTO ' . $this->insert . ' ';
                    break;
            case (isset($this->update)):
                if (empty($this->update))
                {
                    return $this->message->setMessage('You missed argument for update', 2);
                }
                $query = 'UPDATE ' . $this->update  . ' ';
                    break;
            case (isset($this->delete)):
                if (empty($this->delete))
                {
                    return $this->message->setMessage('You missed argument for delete', 2);
                }
                $query = 'DELETE ' . $this->delete  . ' ';
                    break;
            default: return $this->message->setMessage('You missed first operator', 2);
        }	

        if ($this->from)
        {
            if (empty($this->from))
            {
                return false;
            }
            $query .= 'FROM ' . $this->from . ' ';
        }

        if ($this->values)
        {
            if (empty($this->values))
            {
                return false;
            }
            $query .= 'VALUES (' . $this->values . ') ';
        }

        if ($this->set)
        {
            if (empty($this->set))
            {
                return false;
            }
            $query .= 'SET ' . $this->set  . ' ';
        }

        if ($this->where)
        {
            if (!is_array($this->where) or !count($this->where))
            {
                return false;
            }
            $query .= 'WHERE ' . implode(' AND ', $this->where) . ' ';
        }

        return $query;
    }

}
