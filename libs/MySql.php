<?php

Class MySql extends Sql
{
    private $userName;
    private $dbName;
    private $hostName;
    private $password;

    public function __construct()
    {
        parent::__construct();
        $this->hostName = 'localhost';
        $this->dbName = 'user1';
        $this->userName = 'root';
        $this->password = '';
        if (!($link = mysql_connect($this->hostName, $this->userName, $this->password))) {
            die(CONNECTIONERROR . mysql_error());
        }
    }

    public function exec()
    {
        $query = parent::exec();
        if (!mysql_select_db($this->dbName)) {
            die('Ошибка выбора базы данных: ' . mysql_error());
        }
        $result = mysql_query($query);
        if (!$result) {
            //die('Неверный запрос: ' . mysql_error());
        }
        $res = [];
        while ($row = mysql_fetch_assoc($result))
        {
            $res[] = $row;
        }
        return $res;
    }
}

