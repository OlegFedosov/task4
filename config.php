<?php

//directory path to save files
//define("DIRPATH", "/usr/home/user3/public_html/MYPHP/task1/files/");
define("DIRPATH", "/OpenServer/domains/task1/files/");

define("UPLOADSUCCESS", 'File uploaded!');
define("DELETESUCCESS", 'File deleted!');
define("UPLOADFALL", 'Something went wrong, file is not uploaded');
define("DELETEFALL", 'Something went wrong, file is not deleted');
define("EMPTYFILESDIR", 'No uploaded files yet');
