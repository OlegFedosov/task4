<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">
<p class="message"><?php  echo $message['text'] ; ?></p>
    <div class="row">
        <div class="col-md-6">
            <a class="row" href="?select_my=user3">SELECT MySQL</a>
            <a class="row" href="?insert_my=user3&data=some_text">INSERT MySQL</a>
            <a class="row" href="?update_my=user3&data=some_new_text">UPDATE MySQL</a>
            <a class="row" href="?select_my=user3&data=some_text">DELETE MySQL</a>
        </div>
        <div class="col-md-6">
            <a class="row" href="?select_pg=user3">SELECT PgSQL</a>
            <a class="row" href="?insert_pg=user3&data=some_text">INSERT PgSQL</a>
            <a class="row" href="?update_pg=user3&data=some_new_text">UPDATE PgSQL</a>
            <a class="row" href="?select_pg=user3&data=some_text">DELETE PgSQL</a>
        </div>
    </div>
    <?php if (isset($res) and is_array($res) and count($res)): ?>
        <?php foreach ($res as $value): ?>
            <?php echo $value['KEY'] , '=>', $value['VALUE'] ; ?>
        <?php endforeach; ?>
    <?php endif; ?>



</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>











