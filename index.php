<?php
ini_set('error_reporting', E_ALL & ~E_DEPRECATED);
include('config.php');
include('libs/Message.php');
include('libs/Sql.php');
include('libs/MySql.php');
include('libs/PgSql.php');


$mySql = new MySql();
$pgSql = new PgSql();

switch (true)
{
    case (isset($_GET['select_my']) and !empty($_GET['select_my'])):
        $res = $mySql->select('`KEY`', '`VALUE`')->from('`MY_TEST`')->where('`KEY`', '=', 'user3')->exec();
            break;
    case (isset($_GET['select_pg']) and !empty($_GET['select_pg'])):
        $res = $mySql->select('`KEY`', '`VALUE`')->from('`MY_TEST`')->where('`KEY`', '=', 'user3')->exec();
        break;
    case (isset($_GET['update_my']) and !empty($_GET['update_my'])):
        $mySql->update('MY_TEST')->set(['id' => 4, 'name' => 'tttt'])->where('id', '=', 3)->exec();
        break;
    case (isset($_GET['update_pg']) and !empty($_GET['update_pg'])):
        $mySql->update('MY_TEST')->set(['id' => 4, 'name' => 'tttt'])->where('id', '=', 3)->exec();
        break;
    case (isset($_GET['insert_my']) and !empty($_GET['insert_my'])):
        $mySql->insert('MY_TEST', ['id', 'name'])->values([4, 'tttt'])->exec();
        break;
    case (isset($_GET['insert_pg']) and !empty($_GET['insert_pg'])):
        $mySql->insert('MY_TEST', ['id', 'name'])->values([4, 'tttt'])->exec();
        break;
    case (isset($_GET['delete_my']) and !empty($_GET['delete_my'])):
        $mySql->delete()->from()->where()->exec();
        break;
    case (isset($_GET['delete_pg']) and !empty($_GET['delete_pg'])):
        $mySql->delete()->from()->where()->exec();
        break;
    //default: $res = $mySql->select('`KEY`', '`VALUE`')->from('`MY_TEST`')->exec();

}

$messageObj = new Message();
$message = $messageObj->getMessage();
include('templates/index.php');
